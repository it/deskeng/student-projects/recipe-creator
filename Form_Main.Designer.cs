﻿namespace RecipeCreator
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel_MainLayout = new TableLayoutPanel();
            panel_UserInfo = new Panel();
            button_Write = new Button();
            label_AppName = new Label();
            textBox_AppName = new TextBox();
            label_PackagerName = new Label();
            textBox_PackagerName = new TextBox();
            panel_Instructions = new Panel();
            panel_InstructionsControls = new Panel();
            button_DelRow = new Button();
            button_AddRow = new Button();
            tableLayoutPanel_Instructions = new TableLayoutPanel();
            tableLayoutPanel_MainLayout.SuspendLayout();
            panel_UserInfo.SuspendLayout();
            panel_Instructions.SuspendLayout();
            panel_InstructionsControls.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel_MainLayout
            // 
            tableLayoutPanel_MainLayout.AutoSize = true;
            tableLayoutPanel_MainLayout.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tableLayoutPanel_MainLayout.ColumnCount = 1;
            tableLayoutPanel_MainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel_MainLayout.Controls.Add(panel_UserInfo, 0, 0);
            tableLayoutPanel_MainLayout.Controls.Add(panel_Instructions, 0, 1);
            tableLayoutPanel_MainLayout.Dock = DockStyle.Fill;
            tableLayoutPanel_MainLayout.Location = new Point(0, 0);
            tableLayoutPanel_MainLayout.Name = "tableLayoutPanel_MainLayout";
            tableLayoutPanel_MainLayout.RowCount = 3;
            tableLayoutPanel_MainLayout.RowStyles.Add(new RowStyle());
            tableLayoutPanel_MainLayout.RowStyles.Add(new RowStyle());
            tableLayoutPanel_MainLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel_MainLayout.Size = new Size(594, 622);
            tableLayoutPanel_MainLayout.TabIndex = 0;
            // 
            // panel_UserInfo
            // 
            panel_UserInfo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            panel_UserInfo.BorderStyle = BorderStyle.FixedSingle;
            panel_UserInfo.Controls.Add(button_Write);
            panel_UserInfo.Controls.Add(label_AppName);
            panel_UserInfo.Controls.Add(textBox_AppName);
            panel_UserInfo.Controls.Add(label_PackagerName);
            panel_UserInfo.Controls.Add(textBox_PackagerName);
            panel_UserInfo.Location = new Point(3, 3);
            panel_UserInfo.Name = "panel_UserInfo";
            panel_UserInfo.Size = new Size(588, 60);
            panel_UserInfo.TabIndex = 0;
            // 
            // button_Write
            // 
            button_Write.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button_Write.Location = new Point(482, 3);
            button_Write.Name = "button_Write";
            button_Write.Size = new Size(101, 23);
            button_Write.TabIndex = 4;
            button_Write.Text = "Write DOCX";
            button_Write.UseVisualStyleBackColor = true;
            button_Write.Click += button_Write_Click;
            // 
            // label_AppName
            // 
            label_AppName.AutoSize = true;
            label_AppName.Location = new Point(3, 35);
            label_AppName.Name = "label_AppName";
            label_AppName.Size = new Size(74, 15);
            label_AppName.TabIndex = 3;
            label_AppName.Text = "Application: ";
            // 
            // textBox_AppName
            // 
            textBox_AppName.Location = new Point(105, 32);
            textBox_AppName.Name = "textBox_AppName";
            textBox_AppName.Size = new Size(275, 23);
            textBox_AppName.TabIndex = 5;
            // 
            // label_PackagerName
            // 
            label_PackagerName.AutoSize = true;
            label_PackagerName.Location = new Point(3, 6);
            label_PackagerName.Name = "label_PackagerName";
            label_PackagerName.Size = new Size(96, 15);
            label_PackagerName.TabIndex = 1;
            label_PackagerName.Text = "Packager Name: ";
            // 
            // textBox_PackagerName
            // 
            textBox_PackagerName.Location = new Point(105, 4);
            textBox_PackagerName.Name = "textBox_PackagerName";
            textBox_PackagerName.Size = new Size(189, 23);
            textBox_PackagerName.TabIndex = 0;
            // 
            // panel_Instructions
            // 
            panel_Instructions.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            panel_Instructions.BorderStyle = BorderStyle.FixedSingle;
            panel_Instructions.Controls.Add(panel_InstructionsControls);
            panel_Instructions.Controls.Add(tableLayoutPanel_Instructions);
            panel_Instructions.Location = new Point(3, 69);
            panel_Instructions.Name = "panel_Instructions";
            panel_Instructions.Size = new Size(588, 355);
            panel_Instructions.TabIndex = 1;
            // 
            // panel_InstructionsControls
            // 
            panel_InstructionsControls.BorderStyle = BorderStyle.FixedSingle;
            panel_InstructionsControls.Controls.Add(button_DelRow);
            panel_InstructionsControls.Controls.Add(button_AddRow);
            panel_InstructionsControls.Dock = DockStyle.Bottom;
            panel_InstructionsControls.Location = new Point(0, 324);
            panel_InstructionsControls.Name = "panel_InstructionsControls";
            panel_InstructionsControls.Size = new Size(586, 29);
            panel_InstructionsControls.TabIndex = 1;
            // 
            // button_DelRow
            // 
            button_DelRow.Location = new Point(99, 3);
            button_DelRow.Name = "button_DelRow";
            button_DelRow.Size = new Size(90, 21);
            button_DelRow.TabIndex = 1;
            button_DelRow.Text = "Delete Row";
            button_DelRow.UseVisualStyleBackColor = true;
            button_DelRow.Click += button_DelRow_Click;
            // 
            // button_AddRow
            // 
            button_AddRow.Location = new Point(3, 3);
            button_AddRow.Name = "button_AddRow";
            button_AddRow.Size = new Size(90, 21);
            button_AddRow.TabIndex = 0;
            button_AddRow.Text = "Add Row";
            button_AddRow.UseVisualStyleBackColor = true;
            button_AddRow.Click += button_AddRow_Click;
            // 
            // tableLayoutPanel_Instructions
            // 
            tableLayoutPanel_Instructions.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            tableLayoutPanel_Instructions.AutoScroll = true;
            tableLayoutPanel_Instructions.ColumnCount = 2;
            tableLayoutPanel_Instructions.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel_Instructions.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel_Instructions.Location = new Point(0, 0);
            tableLayoutPanel_Instructions.Name = "tableLayoutPanel_Instructions";
            tableLayoutPanel_Instructions.RowCount = 2;
            tableLayoutPanel_Instructions.RowStyles.Add(new RowStyle());
            tableLayoutPanel_Instructions.RowStyles.Add(new RowStyle());
            tableLayoutPanel_Instructions.Size = new Size(586, 322);
            tableLayoutPanel_Instructions.TabIndex = 0;
            // 
            // Form_Main
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(594, 622);
            Controls.Add(tableLayoutPanel_MainLayout);
            MinimumSize = new Size(425, 0);
            Name = "Form_Main";
            Text = "Form_Main";
            tableLayoutPanel_MainLayout.ResumeLayout(false);
            panel_UserInfo.ResumeLayout(false);
            panel_UserInfo.PerformLayout();
            panel_Instructions.ResumeLayout(false);
            panel_InstructionsControls.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel_MainLayout;
        private Panel panel_UserInfo;
        private TextBox textBox_PackagerName;
        private Label label_PackagerName;
        private Label label_AppName;
        private TextBox textBox_AppName;
        private Button button_Write;
        private Panel panel_Instructions;
        private TableLayoutPanel tableLayoutPanel_Instructions;
        private Panel panel_InstructionsControls;
        private Button button_DelRow;
        private Button button_AddRow;
    }
}