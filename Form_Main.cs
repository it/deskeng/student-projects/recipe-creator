﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace RecipeCreator
{
    public partial class Form_Main : Form
    {
        WordprocessingDocument? document;
        Body? docBody;

        private List<InstructionStep> steps = [];

        public Form_Main()
        {
            InitializeComponent();
        }

        private void GenerateDOCX()
        {
            document = WordprocessingDocument.Create("C:\\Users\\drfff9\\Desktop\\test.docx", DocumentFormat.OpenXml.WordprocessingDocumentType.Document);

            MainDocumentPart mainPart = document.MainDocumentPart ?? document.AddMainDocumentPart();
            mainPart.Document ??= new Document();
            mainPart.Document.Body ??= mainPart.Document.AppendChild(new Body());

            docBody = document!.MainDocumentPart!.Document!.Body!;
        }


        // DOCX formatted writing example/test functions

        private void TestWrite()
        {
            GenerateDOCX();

            if (document is null || docBody is null)
            {
                Debug.WriteLine("Document failed to initialize", "Error");
                return;
            }

            //Paragraph para = docBody.AppendChild(new Paragraph());
            //Run run = para.AppendChild(new Run());
            //run.AppendChild(new Text("Test docx!"));

            //Paragraph para2 = docBody.AppendChild(new Paragraph());
            //Run run2 = para2.AppendChild(new Run());
            //run2.AppendChild(new Text("Paragraph 2: Test docx!"));


            //Table packagerInfo = docBody.AppendChild(new Table());

            TableProperties tableProperties = new TableProperties(
                new TableBorders(
                    new TopBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new BottomBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new LeftBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new RightBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new InsideHorizontalBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new InsideVerticalBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    }
                )
            );


            //TableRow row1 = packagerInfo.AppendChild(new TableRow());

            //TableCell left = row1.AppendChild(new TableCell());
            //left.AppendChild(new Paragraph(new Run(new Text("Packaged By:"))));

            //TableCell right = row1.AppendChild(new TableCell());
            //right.AppendChild(new Paragraph(new Run(new Text(textBox_PackagerName.Text))));

            //TableRow row2 = packagerInfo.AppendChild(new TableRow());

            //TableCell pic = row2.AppendChild(new TableCell(GetImage()));

            RunProperties bold = new RunProperties(new Bold());

            Paragraph[][] tableItems = {
                [GetParagraph("Packaged By: ", bold), GetParagraph(textBox_PackagerName.Text)],
                [GetImage("C:\\Users\\drfff9\\Desktop\\Test.bmp"), GetImage("C:\\Users\\drfff9\\Desktop\\Test2.bmp")]
            };

            Table packagerInfo = new Table();
            packagerInfo.AppendChild(tableProperties);
            AddParagraphsToTable(ref packagerInfo, tableItems);

            docBody.AppendChild(packagerInfo);

            document.Save();
            document.Dispose();
        }

        private void WriteFormattedDOCX()
        {
            GenerateDOCX();

            if (document is null || docBody is null)
            {
                Debug.WriteLine("Document failed to initialize", "Error");
                return;
            }

            ParagraphProperties blankParaProp = new();
            ParagraphProperties titleParaProp = new ParagraphProperties(new ParagraphStyleId() { Val = "Title" });

            RunProperties blankRunProp = new();
            //RunProperties title = new RunProperties(new FontSize() { Val = "64" });
            RunProperties header1 = new RunProperties(new FontSize() { Val = "32" }, new Bold());

            TableProperties tableBoarders = new TableProperties(
                new TableBorders(
                    new TopBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new BottomBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new LeftBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new RightBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new InsideHorizontalBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    },
                    new InsideVerticalBorder()
                    {
                        Val = new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 6
                    }
                )
            );

            docBody.AppendChild(GetParagraph("Recipe for " + textBox_AppName.Text, titleParaProp));
            docBody.AppendChild(GetParagraph("Document Information", header1));

            Table docInformation = new Table(tableBoarders);
            AddParagraphsToTable(ref docInformation,
                [[GetParagraph("Packaged By:", MakeBold()), GetParagraph(textBox_PackagerName.Text)],
                [GetParagraph("Creation Date:", MakeBold()), GetParagraph(DateTime.Now.ToString())]]);

            docBody.AppendChild(docInformation);

            document.Save();
            document.Dispose();
        }
        

        #region OpenXML helper functions

        private Paragraph GetParagraph(string text)
        {
            return new Paragraph(new Run(new Text(text)));
        }

        private Paragraph GetParagraph(string text, RunProperties runProp)
        {

            return new Paragraph(new Run(runProp, new Text(text)));
        }

        private Paragraph GetParagraph(string text, ParagraphProperties paraProp)
        {
            return new Paragraph(paraProp, new Run(new Text(text)));
        }

        private Paragraph GetImage(string imagePath)
        {
            if (document is null) return GetParagraph("Error: Document null");

            imagePath = Path.GetFullPath(imagePath);
            if (!File.Exists(imagePath)) return GetParagraph("Error: Image not found");

            ImagePart imagePart = document.MainDocumentPart!.AddImagePart(ImagePartType.Bmp);
            using (FileStream stream = new FileStream(imagePath, FileMode.Open))
            {
                imagePart.FeedData(stream);
            }

            // Taken from the Microsoft example for how to add images (not fun)
            var element =
            new Drawing(
                new DW.Inline(
                    new DW.Extent() { Cx = 990000L, Cy = 792000L },

                    new DW.EffectExtent()
                    {
                        LeftEdge = 0L,
                        TopEdge = 0L,
                        RightEdge = 0L,
                        BottomEdge = 0L
                    },

                    new DW.DocProperties()
                    {
                        Id = (UInt32Value)1U,
                        Name = "Picture 1"
                    },

                    new DW.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoChangeAspect = true }),

                    new A.Graphic(
                        new A.GraphicData(
                            new PIC.Picture(
                                new PIC.NonVisualPictureProperties(
                                    new PIC.NonVisualDrawingProperties()
                                    {
                                        Id = (UInt32Value)0U,
                                        Name = "New Bitmap"
                                    },

                                    new PIC.NonVisualPictureDrawingProperties()
                                ),

                                new PIC.BlipFill(
                                    new A.Blip(new A.BlipExtensionList(new A.BlipExtension() { Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" }))
                                    {
                                        Embed = document.MainDocumentPart!.GetIdOfPart(imagePart),
                                        CompressionState = A.BlipCompressionValues.Print
                                    },

                                    new A.Stretch(new A.FillRectangle())
                                ),

                                new PIC.ShapeProperties(
                                    new A.Transform2D(
                                        new A.Offset() { X = 0L, Y = 0L },
                                        new A.Extents() { Cx = 990000L, Cy = 792000L }
                                    ),

                                    new A.PresetGeometry(new A.AdjustValueList()) { Preset = A.ShapeTypeValues.Rectangle }
                                )
                            )
                        )
                        { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" }
                    )
                )
                {
                    DistanceFromTop = (UInt32Value)0U,
                    DistanceFromBottom = (UInt32Value)0U,
                    DistanceFromLeft = (UInt32Value)0U,
                    DistanceFromRight = (UInt32Value)0U,
                    EditId = "50D07946"
                }
            );

            return new Paragraph(new Run(element));
        }

        private RunProperties MakeBold()
        {
            return new RunProperties(new Bold());
        }


        private void AddParagraphsToTable(ref Table table, Paragraph[][] contents)
        {
            foreach (Paragraph[] line in contents)
            {
                TableRow row = table.AppendChild(new TableRow());
                foreach (Paragraph item in line)
                {
                    row.AppendChild(new TableCell(item));
                }
            }
        }

        #endregion

        #region Click handlers

        private void button_Write_Click(object sender, EventArgs e)
        {
            WriteFormattedDOCX();
        }

        private void button_AddRow_Click(object sender, EventArgs e)
        {
            tableLayoutPanel_Instructions.RowCount++;

            RichTextBox rtb = new RichTextBox() { Dock = DockStyle.Fill };
            tableLayoutPanel_Instructions.Controls.Add(rtb);

            PictureBox pb = new PictureBox() { Dock = DockStyle.Fill };
            pb.SizeMode = PictureBoxSizeMode.Zoom;
            pb.MouseDoubleClick += pictureBox_MouseDoubleClick;
            tableLayoutPanel_Instructions.Controls.Add(pb);

            steps.Add(new InstructionStep(rtb, pb));
        }

        private void button_DelRow_Click(object sender, EventArgs e)
        {
            if (steps.Count <= 0) return;

            steps.Last().instructions.Dispose();
            steps.Last().image.Dispose();
            steps.Remove(steps.Last());
        }
        private void pictureBox_MouseDoubleClick(object? sender, MouseEventArgs e)
        {
            PictureBox pb = (sender as PictureBox)!;

            OpenFileDialog ofd = new OpenFileDialog();
            
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            string codecFilter = "Image Files|";
            foreach (ImageCodecInfo codec in codecs)
            {
                codecFilter += codec.FilenameExtension + ";";
            }
            ofd.Filter = codecFilter;

            if (ofd.ShowDialog() == DialogResult.Cancel) return;

            string image_path = Path.GetFullPath(ofd.FileName);

            pb.ImageLocation = image_path;
        }

        #endregion

    }
}
