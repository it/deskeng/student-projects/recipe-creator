﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeCreator
{
    internal class InstructionStep(RichTextBox rtb, PictureBox pb)
    {
        public RichTextBox instructions = rtb;
        public PictureBox image = pb;
    }
}
